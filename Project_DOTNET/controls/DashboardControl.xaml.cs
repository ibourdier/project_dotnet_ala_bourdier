﻿using Project_DOTNET.services;
using Project_DOTNET.utils;
using Project_DOTNET.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_DOTNET.controls
{
    /// <summary>
    /// Logique d'interaction pour DashboardControl.xaml
    /// </summary>
    public partial class DashboardControl : UserControl
    {
        private ListContactControl listContact;
        public DashboardControl()
        {
            InitializeComponent();
            listContact = new ListContactControl(this);
            colListContact.Children.Add(listContact);

            RetrieveMessageTask task = RetrieveMessageTask.getInstance();
            task.Start();
            task.addMessageListener(listMessage);
            task.addContactListener(listContact);
        }

        public void changeContact(Contact contact)
        {
            listMessage.changeContact(contact);
            if(contact != null && MessageService.countMessage(contact) > 1)
            {
                listMessage.enabledListMessage(true);
            }
        }

    }
}
