﻿using MaterialDesignThemes.Wpf;
using Project_DOTNET.controls.interfaces;
using Project_DOTNET.models;
using Project_DOTNET.services;
using Project_DOTNET.utils;
using Project_DOTNET.viewModels;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_DOTNET.controls
{
    /// <summary>
    /// Logique d'interaction pour ListContactControl.xaml
    /// </summary>
    public partial class ListContactControl : UserControl, IContactListener
    {
        private DashboardControl dashboardControl;
        private ListBox listContact;

        public DashboardControl DashboardControl { get => dashboardControl; set => dashboardControl = value; }

        public ListContactControl()
        {

        }

        public ListContactControl(DashboardControl dashboardControl)
        {
            InitializeComponent();
            NavigationListContactModel navigation = new NavigationListContactModel(this);
            this.DataContext = navigation;
            this.dashboardControl = dashboardControl;
            ((ListContactViewModel)((NavigationListContactModel)this.DataContext).SelectedViewModel).addContactListener(this);
        }

        private void removeSelectedItem(object sender, RoutedEventArgs e)
        {
            /*Contact contact = (Contact)listContact.SelectedItem;
            ContactService.removeContact(contact);
            myList.Remove(contact);*/
        }

        public void refreshListContact(List<Contact> newContacts)
        {
            // Not execute when addContactControl is open
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                if (this.DataContext.GetType() == typeof(NavigationListContactModel))
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                   ((ListContactViewModel)((NavigationListContactModel)this.DataContext).SelectedViewModel).refreshListContact(newContacts)));
                }
            }));
        }

        public void goToEndOfList()
        {
            listContact.SelectedIndex = listContact.Items.Count - 1;
            listContact.ScrollIntoView(listContact.SelectedItem);
        }

        private void ListContact_Loaded(object sender, RoutedEventArgs e)
        {
            listContact = sender as ListBox;
        }

        public void changeContact(Contact contact)
        {
            dashboardControl.changeContact(contact);
        }
    }

}
