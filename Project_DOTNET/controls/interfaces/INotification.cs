﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_DOTNET.controls.interfaces
{
    interface INotification
    {
        void notify(String msg);
    }
}
