﻿using Project_DOTNET.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_DOTNET.controls.interfaces
{
    public interface IRetrieveMessageListener
    {
        void refresh(List<Message> newMessages);
        void goToEndOfList();
    }
}
