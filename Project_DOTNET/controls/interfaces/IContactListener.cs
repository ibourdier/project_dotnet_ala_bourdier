﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_DOTNET.controls.interfaces
{
    public interface IContactListener
    {
        void refreshListContact(List<Contact> contact);
        void goToEndOfList();
        void changeContact(Contact contact);
    }
}
