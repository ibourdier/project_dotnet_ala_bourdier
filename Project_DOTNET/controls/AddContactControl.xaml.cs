﻿using Project_DOTNET.controls.interfaces;
using Project_DOTNET.models;
using Project_DOTNET.services;
using Project_DOTNET.utils;
using Project_DOTNET.viewModels;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_DOTNET.controls
{
    /// <summary>
    /// Interaction logic for AddContactControl.xaml
    /// </summary>
    public partial class AddContactControl : UserControl
    {

        public AddContactControl()
        {
            InitializeComponent();
        }

        private void addContact(object sender, RoutedEventArgs e)
        {
            String username = userName.Text;

            err.Text = validateForm(username);
            if (err.Text.Length <= 0)
            {

                List<Contact> contacts = ContactService.findContactByUsername(username);
                if(contacts.Count > 0)
                {
                    err.Text = "Ce contact est déjà dans votre liste";
                }
                else
                {
                    RSAParameters pubKey, privKey;
                    Crypto cryptoUtil = Crypto.getInstance();
                    cryptoUtil.generateRSA(out privKey, out pubKey);

                    String currentUser = Properties.Settings.Default.username;
                    String token = Properties.Settings.Default.token;

                    String txtMessage = currentUser + "[|]" + Message.Type.PING + "[|]" + cryptoUtil.keyRSAToString(pubKey);
                    RequestNewMessage request = new RequestNewMessage(username, txtMessage);


                    IRestResponse<Message> response = ApiService.getInstance().sendMessage(request, token);

                    if ((int)response.StatusCode == 201)
                    {
                        Message newMessage = response.Data;
                        newMessage.Receiver = username;
                        MessageService.saveMessage(newMessage);
                        Message.Type type = newMessage.getType();
                        Contact newContact = new Contact(username);
                        ContactService.saveContact(newContact);

                        //Return to list contact
                        ((AddContactViewModel)this.DataContext).NavigationListContact.closeAddContact();
                    }
                    else
                    {
                        err.Text = response.Content;
                    }
                }
            }
        }
        private String validateForm(String username)
        {
            if (username.Length < 1) return "Pseudo trop court";
            return "";
        }
    }
}
