﻿using Project_DOTNET.controls.interfaces;
using Project_DOTNET.models;
using Project_DOTNET.services;
using Project_DOTNET.utils;
using Project_DOTNET.viewModels;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_DOTNET.controls
{
    /// <summary>
    /// Logique d'interaction pour ListMessageControl.xaml
    /// </summary>
    public partial class ListMessageControl : UserControl, IRetrieveMessageListener
    {

        public ListMessageControl()
        {
            InitializeComponent();
            ListMessageViewModel viewModel = new ListMessageViewModel();
            viewModel.addContactListener(this);
            this.DataContext = viewModel;
            goToEndOfList();
        }

        public void changeContact(Contact selectedContact)
        {
            ((ListMessageViewModel)this.DataContext).SelectedContact = selectedContact;
            if (selectedContact != null)
            {
                ((ListMessageViewModel)this.DataContext).refreshList(selectedContact);
                goToEndOfList();
            }
        }

        public void refresh(List<Message> newMessages)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() => { ((ListMessageViewModel)this.DataContext).refreshList(newMessages); }));
            if(newMessages.Count > 0)
            {
                goToEndOfList();
            }
        }

        public void goToEndOfList()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() => { listMessage.SelectedIndex = listMessage.Items.Count - 1; }));
            Application.Current.Dispatcher.BeginInvoke(new Action(() => { listMessage.ScrollIntoView(listMessage.SelectedItem); }));
        }
        
        public void enabledListMessage(bool enabled)
        {
            ((ListMessageViewModel)this.DataContext).EnabledListMessage = enabled;
        }
    }
}
