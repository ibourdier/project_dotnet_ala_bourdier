﻿using Dapper;
using Project_DOTNET.models;
using Project_DOTNET.utils;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_DOTNET.services
{
    class MessageService
    {
        public static List<Message> getMessages(Contact contact)
        {
            String currentUser = Properties.Settings.Default.username;
            return SqlDataAccess.getInstance().Connection.Table<Message>().Where(x =>
                ((x.Author == currentUser && x.Receiver == contact.Username) || (x.Author == contact.Username && x.Receiver == currentUser)) 
                    && x.CreatedBy == currentUser).ToList();
        }

        public static void updateMessage(Message message)
        {
            SqlDataAccess.getInstance().Connection.Update(message);
        }

        public static void saveMessage(Message message)
        {
            String currentUser = Properties.Settings.Default.username;
            message.CreatedBy = currentUser;
            SqlDataAccess.getInstance().Connection.Insert(message);
        }

        public static int countMessage(Contact contact)
        {
            String currentUser = Properties.Settings.Default.username;
            return SqlDataAccess.getInstance().Connection.Table<Message>().Count(x =>
                ((x.Author == currentUser && x.Receiver == contact.Username) || (x.Author == contact.Username && x.Receiver == currentUser))
                && x.CreatedBy == currentUser);
        }

        public static List<Contact> getContact(Message message)
        {
            string currentUser = Properties.Settings.Default.username;
            string username = (message.Author != currentUser) ? message.Author : message.Receiver;
            return SqlDataAccess.getInstance().Connection.Table<Contact>().Where(x => x.CreatedBy == currentUser && x.Username == username).ToList();

        }

        internal static void removeMessageByContact(Contact contactRemoved)
        {
            string currentUser = Properties.Settings.Default.username;
            SqlDataAccess.getInstance().Connection.Table<Message>()
                .Delete(x => x.CreatedBy == currentUser 
                    && (x.Author == contactRemoved.Username || x.Receiver == contactRemoved.Username));
        }
    }
}
