﻿using Project_DOTNET.models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_DOTNET.services
{
    class ApiService
    {
        private String urlAPI;
        private static ApiService instance;
        private RestClient client;

        public static ApiService getInstance()
        {
            if(instance == null)
            {
                instance = new ApiService();
                instance.urlAPI = ConfigurationManager.AppSettings["urlAPI"];
                instance.client = new RestClient(instance.urlAPI);
            }
            return instance;
        }

        public IRestResponse register(String username, String password)
        {
            RestRequest request = new RestRequest("api/createUser", Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddJsonBody(new { username= username, password=password});
            return client.Execute(request);
        }

        public IRestResponse<ResponseLogin> login(String username, String password)
        {
            RestRequest request = new RestRequest("api/login", Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddJsonBody(new { username = username, password = password });
            return client.Execute<ResponseLogin>(request);
        }

        public IRestResponse<Message> sendMessage(RequestNewMessage body, String token)
        {
            RestRequest request = new RestRequest("api/sendMsg", Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddJsonBody(new { receiver = body.Receiver, message = body.Message });
            return client.Execute<Message>(request);
        }

        public IRestResponse<List<Message>> fetchMessages(String token)
        {
            RestRequest request = new RestRequest("api/fetchMessages", Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", "Bearer " + token);
            return client.Execute<List<Message>>(request);
        }
    }
}
