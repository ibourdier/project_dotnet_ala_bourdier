﻿using Project_DOTNET.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using SQLite;

namespace Project_DOTNET.services
{
    class ContactService
    {
        public static List<Contact> getContacts()
        {
            String currentUser = Properties.Settings.Default.username;
            return SqlDataAccess.getInstance().Connection.Table<Contact>().Where(x => x.CreatedBy == currentUser).ToList();
        }

        public static void removeContact(Contact contact)
        {
            String currentUser = Properties.Settings.Default.username;
            SqlDataAccess.getInstance().Connection.Table<Contact>().Delete(x => x.CreatedBy == currentUser && x.Id == contact.Id);
        }

        public static void updateContact(Contact contact)
        {
            SqlDataAccess.getInstance().Connection.Update(contact);
        }

        public static void saveContact(Contact contact)
        {
            String currentUser = Properties.Settings.Default.username;
            contact.CreatedBy = currentUser;
            SqlDataAccess.getInstance().Connection.Insert(contact);
        }

        public static List<Contact> findContactByUsername(String username)
        {
            String currentUser = Properties.Settings.Default.username;
            return SqlDataAccess.getInstance().Connection.Table<Contact>().Where(x => x.Username == username).ToList();
        }
    }
}
