﻿using Project_DOTNET.controls.interfaces;
using Project_DOTNET.models;
using Project_DOTNET.services;
using Project_DOTNET.utils;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Project_DOTNET.viewModels
{
    class ListMessageViewModel : INotifyPropertyChanged
    {
        private BaseCommandWithParameter sendMessageCommand;
        private ObservableCollection<Message> myList;
        private List<IRetrieveMessageListener> listContactListener;
        private Contact selectedContact;
        private bool enabledListMessage = false;
        private String username;

        public event PropertyChangedEventHandler PropertyChanged;

        public Contact SelectedContact {
            get => selectedContact;
            set => selectedContact = value;
        }
        public BaseCommandWithParameter SendMessageCommand { get => sendMessageCommand; set => sendMessageCommand = value; }

        public ObservableCollection<Message> MyList { get => myList;
            set
            {
                myList = value;
                OnPropertyChanged("MyList");
            }
        }

        public ListMessageViewModel()
        {
            sendMessageCommand = new BaseCommandWithParameter(sendMessage);
            listContactListener = new List<IRetrieveMessageListener>();
            username = Properties.Settings.Default.username;
        }

        public void refreshList(Contact contact)
        {
            MyList = new ObservableCollection<Message>(MessageService.getMessages(contact));
            if(myList.Count > 1)
            {
                EnabledListMessage = true;
            }
        }

        public void addContactListener(IRetrieveMessageListener listener)
        {
            this.listContactListener.Add(listener);
        }

        private void goToEndOfList()
        {
            listContactListener.ForEach((listener) =>
            {
                listener.goToEndOfList();
            });
        }

        private void sendMessage(Object parameter)
        {
            TextBox txtMessage = (TextBox)parameter;
            String currentUser = Properties.Settings.Default.username;
            String token = Properties.Settings.Default.token;
            byte[] iv;
            if (txtMessage.Text != "")
            {
                String msg = currentUser + "[|]" + Message.Type.MSG + "[|]" + Crypto.getInstance().encryptAES(txtMessage.Text,
                    Convert.FromBase64String(selectedContact.KeyAES), out iv) + "[|]" + Convert.ToBase64String(iv);
                RequestNewMessage request = new RequestNewMessage(this.selectedContact.Username, msg);
                IRestResponse<Message> response = ApiService.getInstance().sendMessage(request, token);
                if ((int)response.StatusCode == 201)
                {
                    Message newMessage = response.Data;
                    newMessage.Receiver = request.Receiver;
                    MessageService.saveMessage(newMessage);
                    txtMessage.Text = "";
                    MyList.Add(newMessage);
                    goToEndOfList();
                }
            }
        }

        public void refreshList(List<Message> messages)
        {
            if (MyList != null)
            {
                messages.ForEach((message) =>
                {
                    if (message.Author == selectedContact.Username || message.Receiver == selectedContact.Username)
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(() => { refreshList(selectedContact); }));
                        Application.Current.Dispatcher.BeginInvoke(new Action(() => { goToEndOfList(); }));
                    }
                });
            }
        }

        public bool EnabledListMessage
        {
            get => enabledListMessage;
            set
            {
                enabledListMessage = value;
                OnPropertyChanged("EnabledListMessage");
            }
        }


        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
