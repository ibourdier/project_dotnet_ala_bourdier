﻿using Project_DOTNET.models;
using Project_DOTNET.services;
using Project_DOTNET.utils;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Project_DOTNET.viewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        private BaseCommandWithParameter submitLoginCommand;
        private BaseCommand navigateToRegisterCommand;
        private string username;
        private string password;
        private string err;
        private readonly UIElement view;
        public event PropertyChangedEventHandler PropertyChanged;

        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public BaseCommandWithParameter SubmitLoginCommand { get => submitLoginCommand; set => submitLoginCommand = value; }
        public string Err {
            get { return err; }
            set { err = value; OnPropertyChanged("Err"); }
        }
        public BaseCommand NavigateToRegisterCommand { get => this.navigateToRegisterCommand; set => this.navigateToRegisterCommand = value; }

        public LoginViewModel(UIElement view)
        {
            this.view = view;
            submitLoginCommand = new BaseCommandWithParameter(submitLogin);
            navigateToRegisterCommand = new BaseCommand(navigateToRegister);
        }

        private String checkErrors(int responseCode)
        {
            switch (responseCode)
            {
                case 401:
                    return "Erreur : Pseudo/Mot de passe incorrect";
                case 200:
                    return "";
                default:
                    return "Erreur : Connexion impossible";
            }
        }

        public void submitLogin(object passwordBox)
        {
            String pw = ((PasswordBox)passwordBox).Password;
            IRestResponse<ResponseLogin> responseLogin = ApiService.getInstance().login(username, pw);
            int rescode = (int)responseLogin.StatusCode;
            Err = checkErrors(rescode);
            if (rescode == 200)
            {
                Properties.Settings.Default.token = responseLogin.Data.AccessToken;
                Properties.Settings.Default.username = responseLogin.Data.Username;
                Properties.Settings.Default.Save();
                ((NavigationViewModel)((MainWindow)Window.GetWindow(view)).DataContext).triggerSnackbar("Connexion réussie. Bonjour " + Properties.Settings.Default.username + " !");
                ((NavigationViewModel)((MainWindow)Window.GetWindow(view)).DataContext).SelectedViewModel = new DashboardViewModel();
            }
        }

        public void navigateToRegister()
        {
            ((NavigationViewModel)((MainWindow)Window.GetWindow(view)).DataContext).SelectedViewModel = new RegisterViewModel(view);
        }

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
