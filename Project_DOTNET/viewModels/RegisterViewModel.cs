﻿using Project_DOTNET.models;
using Project_DOTNET.services;
using Project_DOTNET.utils;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Project_DOTNET.viewModels
{
    class RegisterViewModel : INotifyPropertyChanged
    {
        private string username;
        private string password;
        private string confirmPassword;
        private string err;
        private bool modalIsOpen;
        private BaseCommand navigateToLoginCommand;
        private UIElement view;
        private BaseCommandWithParameter submitRegisterCommand;
        private BaseCommandWithParameter getLoggedCommand;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string ConfirmPassword { get => confirmPassword; set => confirmPassword = value; }
        public string Err
        {
            get { return err; }
            set { err = value; OnPropertyChanged("Err"); }
        }
        public bool ModalIsOpen { get => modalIsOpen; set { modalIsOpen = value; OnPropertyChanged("ModalIsOpen"); }  }
        public BaseCommand NavigateToLoginCommand { get => this.navigateToLoginCommand; set => this.navigateToLoginCommand = value; }
        public BaseCommandWithParameter SubmitRegisterCommand { get => submitRegisterCommand; set => submitRegisterCommand = value; }
        public BaseCommandWithParameter GetLoggedCommand { get => getLoggedCommand; set => getLoggedCommand = value; }

        public RegisterViewModel(UIElement view)
        {
            this.view = view;
            this.submitRegisterCommand = new BaseCommandWithParameter(submitRegister);
            this.getLoggedCommand = new BaseCommandWithParameter(getLogged);
            this.navigateToLoginCommand = new BaseCommand(navigateToLogin);
            username = "";
        }

        private String validateForm(String username, String password, String passwordConfirm)
        {
            if (username.Length < 4) { return "Username pas assez long"; }
            if (password != passwordConfirm) { return "Les mots de passes ne sont pas identiques"; }
            if (password.Length < 4) { return "Le mot de passe doit être de minimum 4 caractères"; }
            return "";
        }

        private String checkErrors(IRestResponse response)
        {
            int responseCode = (int)response.StatusCode;
            switch (responseCode)
            {
                case 400:
                    return response.Content;
                case 401:
                    return "Erreur : Pseudo/Mot de passe incorrect";
                case 200 | 201:
                    return "";
                default:
                    return "Erreur : Connexion impossible";
            }
        }

        public void submitRegister(object parameter)
        {
            String uname = username;
            object[] passwords = ((object[])parameter);
            String pw = ((PasswordBox)passwords[0]).Password;
            String pwConf = ((PasswordBox)passwords[1]).Password;

            Err = validateForm(uname, pw, pwConf);
            if (Err.Length == 0)
            {
                IRestResponse responseRegister = ApiService.getInstance().register(uname, pw);
                int rescode = (int)responseRegister.StatusCode;

                Err = checkErrors(responseRegister);
                if (rescode == 201)
                {
                    ModalIsOpen = true;
                }
            }
        }

        public void getLogged(object password)
        {
            String uname = username;
            String pw = ((PasswordBox)password).Password;
            IRestResponse<ResponseLogin> responseLogin = ApiService.getInstance().login(uname, pw);
            Err = checkErrors(responseLogin);
            if (responseLogin.IsSuccessful)
            {
                Properties.Settings.Default.token = responseLogin.Data.AccessToken;
                Properties.Settings.Default.username = responseLogin.Data.Username;
                Properties.Settings.Default.Save();
                ((NavigationViewModel)((MainWindow)Window.GetWindow(view)).DataContext).SelectedViewModel = new DashboardViewModel();
            }
        }

        public void navigateToLogin()
        {
            ((NavigationViewModel)((MainWindow)Window.GetWindow(view)).DataContext).SelectedViewModel = new LoginViewModel(view);
        }

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
