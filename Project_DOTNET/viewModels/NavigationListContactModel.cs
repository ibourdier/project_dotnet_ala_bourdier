﻿using Project_DOTNET.controls.interfaces;
using Project_DOTNET.services;
using Project_DOTNET.utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Project_DOTNET.viewModels
{
    class NavigationListContactModel : INotifyPropertyChanged
    {
        private BaseCommand addContactCommand;
        private BaseCommand closeContactCommand;
        private BaseCommand disconnectCommand;
        private BaseCommand removeContactCommand;
        private Visibility addBtnVisibility;
        private Visibility closeBtnVisibility;
        private Visibility removeContactBtnVisibility;
        private object selectedViewModel;
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly UIElement view;
        public ListContactViewModel backupSelectedViewModel;
        public object SelectedViewModel
        {
            get { return selectedViewModel; }
            set { selectedViewModel = value; OnPropertyChanged("SelectedViewModel"); }
        }
        
        public BaseCommand AddContactCommand { get => addContactCommand; set => addContactCommand = value; }
        public BaseCommand CloseContactCommand { get => closeContactCommand; set => closeContactCommand = value; }
        public BaseCommand DisconnectCommand { get => disconnectCommand; set => disconnectCommand = value; }
        public BaseCommand RemoveContactCommand { get => removeContactCommand; set => removeContactCommand = value; }

        public Visibility AddBtnVisibility {
            get => addBtnVisibility;
            set {
                addBtnVisibility = value;
                OnPropertyChanged("AddBtnVisibility");
            }
        }
        public Visibility CloseBtnVisibility
        {
            get => closeBtnVisibility;
            set
            {
                closeBtnVisibility = value;
                OnPropertyChanged("CloseBtnVisibility");
            }
        }
        public Visibility RemoveContactBtnVisibility
        {
            get => removeContactBtnVisibility;
            set
            {
                removeContactBtnVisibility = value;
                OnPropertyChanged("RemoveContactBtnVisibility");
            }
        }

        public NavigationListContactModel(UIElement view)
        {
            this.view = view;
            SelectedViewModel = new ListContactViewModel();
            addContactCommand = new BaseCommand(showAddContact);
            removeContactCommand = new BaseCommand(removeContact);
            disconnectCommand = new BaseCommand(disconnectUser);
            AddBtnVisibility = Visibility.Visible;

            closeContactCommand = new BaseCommand(closeAddContact);
            CloseBtnVisibility = Visibility.Collapsed;
        }

        public void removeContact()
        {
            Contact contactSelected = ((ListContactViewModel)SelectedViewModel).SelectedItem;
            ContactService.removeContact(contactSelected);
            MessageService.removeMessageByContact(contactSelected);
            ((ListContactViewModel)SelectedViewModel).refreshListContact(new List<Contact>());
        }
        

        public void showAddContact()
        {
            AddBtnVisibility = Visibility.Collapsed;
            CloseBtnVisibility = Visibility.Visible;
            RemoveContactBtnVisibility = Visibility.Collapsed;
            backupSelectedViewModel = (ListContactViewModel)SelectedViewModel;
            SelectedViewModel = new AddContactViewModel(this);
        }

        public void closeAddContact()
        {
            AddBtnVisibility = Visibility.Visible;
            CloseBtnVisibility = Visibility.Collapsed;
            RemoveContactBtnVisibility = Visibility.Visible;
            SelectedViewModel = backupSelectedViewModel;
            backupSelectedViewModel.refreshListContact(new List<Contact>());
        }

        public void disconnectUser()
        {
            RetrieveMessageTask.getInstance().Stop();
            RetrieveMessageTask.getInstance().unsubscribeAll();
            Properties.Settings.Default.token = null;
            Properties.Settings.Default.username = null;
            Properties.Settings.Default.Save();

            ((NavigationViewModel)((MainWindow)Window.GetWindow(view)).DataContext).triggerSnackbar("Déconnexion réussie. A bientôt !");
            ((NavigationViewModel)((MainWindow)Window.GetWindow(view)).DataContext).SelectedViewModel = new LoginViewModel(((MainWindow)Window.GetWindow(view)));
        }

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
