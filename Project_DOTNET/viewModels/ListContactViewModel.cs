﻿using Project_DOTNET.controls;
using Project_DOTNET.controls.interfaces;
using Project_DOTNET.services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Project_DOTNET.viewModels
{
    class ListContactViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Contact> myList;
        private int selectedIndex;
        private Contact selectedItem;
        private List<IContactListener> listContactListener;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Contact> MyList { get => myList; set
            {
                myList = value;
                OnPropertyChanged("MyList");
            }
        }
        public int SelectedIndex { get => selectedIndex; set => selectedIndex = value; }
        public Contact SelectedItem { get => selectedItem;
            set
            {
                listContactListener.ForEach((listener) =>
                {
                    listener.changeContact((Contact)value);
                });
                selectedItem = value;
            }
        }

        public ListContactViewModel()
        {
            MyList = new ObservableCollection<Contact>(ContactService.getContacts());
            listContactListener = new List<IContactListener>();
        }

        public void refreshListContact(List<Contact> newContacts)
        {
            if (MyList != null)
            {
                MyList = new ObservableCollection<Contact>(ContactService.getContacts());
                goToEndOfList();
            }
        }

        public void addContactListener(IContactListener listener)
        {
            this.listContactListener.Add(listener);
        }

        private void goToEndOfList()
        {
            listContactListener.ForEach((listener) =>
            {
                listener.goToEndOfList();
            });
        }

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
