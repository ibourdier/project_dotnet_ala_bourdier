﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_DOTNET.viewModels
{
    class AddContactViewModel
    {
        private NavigationListContactModel navigationListContact;

        internal NavigationListContactModel NavigationListContact { get => navigationListContact; set => navigationListContact = value; }

        public AddContactViewModel(NavigationListContactModel navigationListContact)
        {
            this.navigationListContact = navigationListContact;
        }

    }
}
