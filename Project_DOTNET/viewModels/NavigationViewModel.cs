﻿using MaterialDesignThemes.Wpf;
using Project_DOTNET.controls.interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Project_DOTNET.viewModels
{
    class NavigationViewModel : INotifyPropertyChanged, INotification
    {
        private object selectedViewModel;
        private SnackbarMessageQueue appMessageQueue;
        private UIElement view;

        public event PropertyChangedEventHandler PropertyChanged;
        public SnackbarMessageQueue AppMessageQueue {
            get { return appMessageQueue; }
            set { appMessageQueue = value; }
        }


        public NavigationViewModel(UIElement view)
        {
            this.view = view;
            AppMessageQueue = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(5000));
            SelectedViewModel = new LoginViewModel(view);
        }

        public object SelectedViewModel
        {
            get { return selectedViewModel; }
            set { selectedViewModel = value; OnPropertyChanged("SelectedViewModel"); }
        }

        private void LoginModel(object obj)
        {
            SelectedViewModel = new LoginViewModel(view);
        }

        public void triggerSnackbar(String msg)
        {
            AppMessageQueue.Enqueue(msg);
        }

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        public void notify(String msg)
        {
            AppMessageQueue.Enqueue(msg);
        }
    }
}
