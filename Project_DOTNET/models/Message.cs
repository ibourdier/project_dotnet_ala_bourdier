﻿using Project_DOTNET.services;
using Project_DOTNET.utils;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_DOTNET.models
{
    [Table("Message")]
    public class Message
    {
        [PrimaryKey, AutoIncrement]
        [Column("id")]
        public long Id { get; set; }
        [Column("receiver")]
        public String Receiver { get; set; }
        [Column("author")]
        public String Author { get; set; }
        [Column("msg")]
        public String Msg { get; set; }
        [Column("dateCreated")]
        public DateTime DateCreated { get; set; }
        [Column("createdBy")]
        public String CreatedBy { get; set; }
        [Column("alreadyReturned")]
        public bool AlreadyReturned { get; set; }
        public bool IsCreatedByLoggedUser { get { return Author == Properties.Settings.Default.username; } set { } }

        public String GetContentDecrypted
        {
            get
            {
                switch (getType())
                {
                    case Type.PING:
                        return "Demande de conversation !";
                    case Type.PONG:
                        return "Réponse positif à la demande !";
                    case Type.MSG:
                        List<Contact> contacts = MessageService.getContact(this);
                        return Crypto.getInstance().decryptAES(getContent(), Convert.FromBase64String(contacts[0].KeyAES), Convert.FromBase64String(this.getIV()));
                    default:
                        return this.getContent();
                }
            }
        }

        public enum Type {PING, PONG, MSG};

        public Message()
        {

        }

        public Type getType()
        {
            String[] splitted = Msg.Split(new String[] {"[|]"}, StringSplitOptions.None);
            return (Type)Enum.Parse(typeof(Type), splitted[1]);
        }

        public String getContent()
        {
            String[] splitted = Msg.Split(new String[] { "[|]" }, StringSplitOptions.None);
            return splitted[2];
        }

        public String getIV()
        {
            String[] splitted = Msg.Split(new String[] { "[|]" }, StringSplitOptions.None);
            return splitted[3];
        }

        public Message(String author, String receiver, String content, bool alreadyReturned)
        {
            this.Author = author;
            this.Receiver = receiver;
            this.AlreadyReturned = alreadyReturned;
            this.Msg = content;
        }
        
    }
}
