﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_DOTNET.models
{
    class RequestNewMessage
    {
        private String receiver;
        private String message;

        public RequestNewMessage()
        {

        }

        public RequestNewMessage(String receiver, String message)
        {
            this.Receiver = receiver;
            this.Message = message;
        }

        public string Receiver { get => receiver; set => receiver = value; }
        public string Message { get => message; set => message = value; }

        public static RequestNewMessage fromMessage(Message msg)
        {
            return new RequestNewMessage(msg.Receiver, msg.Msg);
        }
    }
}
