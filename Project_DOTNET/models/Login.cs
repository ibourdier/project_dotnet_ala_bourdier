﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Project_DOTNET.models
{
    class Login
    {

        public String UsernameValue;
        public String PasswordValue;
        #region Props

        public String username {
            get { return UsernameValue; }
            set { UsernameValue = value; }
        }

        public SecureString SecurePassword { private get; set; }

        public String password
        {
            get { return PasswordValue; }
            set { PasswordValue = value; }
        }

        #endregion
    }
}
