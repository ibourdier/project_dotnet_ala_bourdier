﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_DOTNET
{
    [Table("Contact")]
    public class Contact
    {
        [PrimaryKey, AutoIncrement]
        [Column("id")]
        public long Id { get; set; }
        [Column("username")]
        public String Username { get; set; }
        [Column("createdBy")]
        public String CreatedBy { get; set; }
        [Column("keyAES")]
        public String KeyAES { get; set; }
        [Column("iv")]
        public String IV { get; set; }

        public Contact()
        {

        }

        public Contact(String username)
        {
            this.Username = username;
        }
    }
}
