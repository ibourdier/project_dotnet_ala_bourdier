﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_DOTNET.models
{
    class ResponseLogin
    {
        public String Username { get; set; }
        public String AccessToken { get; set; }
        [DeserializeAs(Name = "expires_in")]
        public int expiresIn { get; set; }
        [DeserializeAs(Name = "refresh_token")]
        public String RefreshToken { get; set; }
    }
}
