﻿using MaterialDesignThemes.Wpf;
using Project_DOTNET.controls;
using Project_DOTNET.models;
using Project_DOTNET.services;
using Project_DOTNET.utils;
using Project_DOTNET.viewModels;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_DOTNET
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public MainWindow()
        {
            InitializeComponent();
            SqlDataAccess.getInstance().initDatabase();
            NavigationViewModel navigation = new NavigationViewModel(this);
            RetrieveMessageTask.getInstance().ListenerNotification = navigation;
            this.DataContext = navigation;

            restoreButton.Visibility = Visibility.Collapsed;
            maximizeButton.Visibility = Visibility.Visible;
        }
        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        protected override void OnStateChanged(EventArgs e)
        {
            //do what you like
            base.OnStateChanged(e);

            if (WindowState == WindowState.Maximized)
            {
                maximizeButton.Visibility = Visibility.Collapsed;
                restoreButton.Visibility = Visibility.Visible;
            }
        }

        private void CloseWindow(object sender, ExecutedRoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void MinimizeWindow(object sender, ExecutedRoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void MaximizeWindow(object sender, ExecutedRoutedEventArgs e)
        {
            this.WindowState = WindowState.Maximized;
            maximizeButton.Visibility = Visibility.Collapsed;
            restoreButton.Visibility = Visibility.Visible;
        }

        private void RestoreWindow(object sender, ExecutedRoutedEventArgs e)
        {
            this.WindowState = WindowState.Normal;
            maximizeButton.Visibility = Visibility.Visible;
            restoreButton.Visibility = Visibility.Collapsed;
        }

        private void TitleBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (TitleBar.IsMouseDirectlyOver)
                    DragMove();
            }
        }
    }
}
