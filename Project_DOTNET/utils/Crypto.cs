﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Project_DOTNET.utils
{
    class Crypto
    {
        private static Crypto instance;
        private String containerName = "chatSecuredStore";

        public static Crypto getInstance()
        {
            if(instance == null)
            {
                instance = new Crypto();
            }
            return instance;
        }

        public void generateRSA(out RSAParameters privKey, out RSAParameters pubKey)
        {
            CspParameters cp = new CspParameters();
            cp.KeyContainerName = containerName + "_" + Properties.Settings.Default.username;

            RSACryptoServiceProvider csp = new RSACryptoServiceProvider(2048, cp);
            privKey = csp.ExportParameters(true);
            pubKey = csp.ExportParameters(false);
        }

        public byte[] generateKeyAES()
        {
            using (AesManaged aes = new AesManaged())
            {
                return aes.Key;
            }
        }

        public String encryptRSA(RSAParameters pubKey, byte[] bytesPlainTextData)
        {
            RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
            csp.ImportParameters(pubKey);

            //apply pkcs#1.5 padding and encrypt our data 
            Byte[] bytesCypherText = csp.Encrypt(bytesPlainTextData, false);

            //we might want a string representation of our cypher text... base64 will do 
            String cypherText = Convert.ToBase64String(bytesCypherText);

            return cypherText;
        }

        public String decryptRSA(RSAParameters privKey, String encodedMessage)
        {
            //first, get our bytes back from the base64 string ... 
            byte[] bytesCypherText = Convert.FromBase64String(encodedMessage);

            //we want to decrypt, therefore we need a csp and load our private key 
            RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
            csp.ImportParameters(privKey);

            //decrypt and strip pkcs#1.5 padding 
            byte[] bytesPlainTextData = csp.Decrypt(bytesCypherText, false);

            //get our original plainText back... 
            return System.Text.Encoding.Unicode.GetString(bytesPlainTextData);

        }

        public String encryptAES(string plainText, byte[] Key, out byte[] IV)
        {
            byte[] encrypted;
            // Create a new AesManaged.    
            using (AesManaged aes = new AesManaged())
            {
                aes.GenerateIV();
                IV = aes.IV;
                // Create encryptor    
                ICryptoTransform encryptor = aes.CreateEncryptor(Key, IV);
                // Create MemoryStream    
                using (MemoryStream ms = new MemoryStream())
                {
                    // Create crypto stream using the CryptoStream class. This class is the key to encryption    
                    // and encrypts and decrypts data from any given stream. In this case, we will pass a memory stream    
                    // to encrypt    
                    using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                    {
                        // Create StreamWriter and write data to a stream    
                        using (StreamWriter sw = new StreamWriter(cs))
                            sw.Write(plainText);
                        encrypted = ms.ToArray();
                    }
                }
            }
            // Return encrypted data    
            return Convert.ToBase64String(encrypted);
        }

        public string decryptAES(String cipherText, byte[] Key, byte[] IV)
        {
            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            string plaintext = null;
            // Create AesManaged    
            using (AesManaged aes = new AesManaged())
            {
                // Create a decryptor    
                ICryptoTransform decryptor = aes.CreateDecryptor(Key, IV);
                // Create the streams used for decryption.    
                using (MemoryStream ms = new MemoryStream(cipherBytes))
                {
                    // Create crypto stream    
                    using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    {
                        // Read crypto stream    
                        using (StreamReader reader = new StreamReader(cs))
                            plaintext = reader.ReadToEnd();
                    }
                }
            }
            return plaintext;
        }

        public String keyRSAToString(RSAParameters rsa)
        {
            //we need some buffer 
            var sw = new System.IO.StringWriter();
            //we need a serializer 
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            //serialize the key into the stream 
            xs.Serialize(sw, rsa);
            //get the string from the stream 
            return sw.ToString();
        }

        public RSAParameters stringToKeyRSA(String rsa)
        {
            //get a stream from the string 
            StringReader sr = new StringReader(rsa);
            //we need a deserializer 
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            //get the object back from the stream 
            return (RSAParameters)xs.Deserialize(sr);
        }
    }
}
