﻿using Project_DOTNET.controls.interfaces;
using Project_DOTNET.models;
using Project_DOTNET.services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Xml;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace Project_DOTNET.utils
{
    class RetrieveMessageTask
    {
        private static RetrieveMessageTask instance;
        private static Timer _timer;
        private List<IRetrieveMessageListener> listenersMessage;
        private List<IContactListener> listenersContact;
        private INotification listenerNotification;

        public INotification ListenerNotification { get => listenerNotification; set => listenerNotification = value; }

        public static RetrieveMessageTask getInstance()
        {
            if (instance == null)
            {
                instance = new RetrieveMessageTask();
                instance.listenersMessage = new List<IRetrieveMessageListener>();
                instance.listenersContact = new List<IContactListener>();
                
            }
            return instance;
        }

        public void Start()
        {
            _timer = new Timer(3000); 
            _timer.Elapsed += new ElapsedEventHandler(_timer_Elapsed);
            _timer.Enabled = true;
        }

        public void Stop()
        {
            _timer.Stop();
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            String currentUser = Properties.Settings.Default.username;
            IRestResponse<List<Message>> response = ApiService.getInstance().fetchMessages(Properties.Settings.Default.token);
            List<Message> newMessages = new List<Message>();
            if((int)response.StatusCode == 200)
            {
                response.Data.ForEach((message) =>
                {
                    if (!message.AlreadyReturned)
                    {
                        message.Receiver = currentUser;
                        MessageService.saveMessage(message);
                        newMessages.Add(message);
                        List<Contact> contactsFind = ContactService.findContactByUsername(message.Author);


                        // If PING message
                        switch (message.getType())
                        {
                            case Message.Type.PING:
                                if (contactsFind.Count == 0)
                                {
                                    byte[] keyAES;
                                    keyAES = Crypto.getInstance().generateKeyAES();
                                    Contact newContact = new Contact(message.Author);
                                    newContact.KeyAES = Convert.ToBase64String(keyAES);
                                    ContactService.saveContact(newContact);
                                    List<Contact> newContacts = new List<Contact>();
                                    newContacts.Add(newContact);
                                    notifyContactListeners(newContacts);

                                    //Create pong
                                    // TODO a mettre dans le ListContactControl quand la liste se fait notifier
                                    RSAParameters pubKey = Crypto.getInstance().stringToKeyRSA(message.getContent());
                                    String keyEncrypted = Crypto.getInstance().encryptRSA(pubKey, System.Text.Encoding.Unicode.GetBytes(newContact.KeyAES));
                                    String txtMessage = currentUser + "[|]" + Message.Type.PONG + "[|]" + keyEncrypted;
                                    RequestNewMessage request = new RequestNewMessage(newContact.Username, txtMessage);

                                    IRestResponse<Message> responseNewMessage = ApiService.getInstance().sendMessage(request, Properties.Settings.Default.token);

                                    if ((int)responseNewMessage.StatusCode == 201)
                                    {
                                        Message newMessage = responseNewMessage.Data;
                                        newMessage.Receiver = newContact.Username;
                                        MessageService.saveMessage(newMessage);
                                        newMessages.Add(newMessage);
                                    }
                                }
                                break;
                            case Message.Type.PONG:
                                if (contactsFind.Count > 0)
                                {
                                    RSAParameters pubKey, privKey;
                                    Crypto.getInstance().generateRSA(out privKey, out pubKey);
                                    Contact contact = contactsFind[0];

                                    //Decrypt AES Key with privKey
                                    String decryptedAES = Crypto.getInstance().decryptRSA(privKey, message.getContent());

                                    contact.KeyAES = decryptedAES;
                                    ContactService.updateContact(contact);
                                    notifyContactListeners(new List<Contact>());
                                }
                                break;

                            case Message.Type.MSG:
                                listenerNotification.notify(message.GetContentDecrypted);
                                break;
                        }
                    }
                });
                notifyMessageListeners(newMessages);
            }
        }

        public void addMessageListener(IRetrieveMessageListener listener)
        {
            this.listenersMessage.Add(listener);
        }

        public void addContactListener(IContactListener listener)
        {
            this.listenersContact.Add(listener);
        }

        public void notifyMessageListeners(List<Message> newMessages)
        {
            listenersMessage.ForEach((listener) =>
            {
                listener.refresh(newMessages);
            });
        }

        public void notifyContactListeners(List<Contact> newContacts)
        {
            listenersContact.ForEach((listener) =>
            {
                listener.refreshListContact(newContacts);
            });
        }

        public void unsubscribeAll()
        {
            listenersMessage.Clear();
            listenersContact.Clear();
        }
    }
}
