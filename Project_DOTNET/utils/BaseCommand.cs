﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Project_DOTNET.utils
{
    public class BaseCommand : ICommand
    {
        private readonly Action actionToExecute;
        public BaseCommand(Action action)
        {
            actionToExecute = action;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }
        public event EventHandler CanExecuteChanged;
        public void Execute(object parameter)
        {
            actionToExecute();
        }
    }

    public class BaseCommandWithParameter : ICommand
    {
        private readonly Action<object> actionToExecute;
        public BaseCommandWithParameter(Action<object> action)
        {
            actionToExecute = action;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }
        public event EventHandler CanExecuteChanged;
        public void Execute(object parameter)
        {
            actionToExecute(parameter);
        }
    }

}
