﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Project_DOTNET.utils
{
    class NavUtil
    {
        public static void navigate(object sender, RoutedEventArgs e, UserControl ctrl, Panel panelContent)
        {
            navigate(ctrl, panelContent);
        }

        public static void navigate(UserControl ctrl, Panel panelContent)
        {
            panelContent.Children.Clear();
            panelContent.Children.Add(ctrl);
        }
    }
}
