﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using SQLite;
using Project_DOTNET.models;

namespace Project_DOTNET.utils
{
    class SqlDataAccess
    {


        private static SqlDataAccess instance;
        private SQLiteConnection connection;

        public SQLiteConnection Connection { get => connection; }

        public static SqlDataAccess getInstance()
        {
            if(instance == null)
            {
                instance = new SqlDataAccess();
                instance.connection = new SQLiteConnection(loadConnectionString());
            }
            return instance;
        }

        public static string loadConnectionString(string id = "default")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }

        public void initDatabase()
        {
            connection.CreateTable<Contact>();
            connection.CreateTable<Message>();
        }


    }
}
